//-------------------------------------

// vertical and horizontal grids
var canvas = document.getElementById('canvas');
var ctx = canvas.getContext('2d'); 
var i,j;
var shape = "circle";
var rotate_Left;
var xangle;
var eu_want_team;

ctx.beginPath();
for (var y=0; y <=500; y +=50) {
    ctx.moveTo(0,y);
    ctx.lineTo(800,y);
    ctx.strokeStyle = "#aeb5b7";
    ctx.lineWidth = 1;

}

    for (var k=50; k <=800; k +=50) {
        ctx.moveTo(k,0);
        ctx.lineTo(k,500);
    }

    ctx.stroke();

//---------------------------------------------

$('button').click(function(){
   eu_want_team = $('#eu_want_team').is(':checked');
//if(eu_want_team === true){
    //alert("esra");
//}

})

//---------------------------------------------

var posX;
var posY;

$(document).ready(function() {
 $('#canvas').click(function(e) {
   var offset = $(this).offset();
    posX = (e.pageX - offset.left);
    posY = (e.pageY - offset.top);
    canvasClicked();
 });
});

function canvasClicked() {
    console.log(shape);
    if (shape == "circle") {
        doCircle();
    }
    else if (shape == "square") {
        doSquare();
    }
    else if (shape == "hexagon") {
        doHexagon();
    }
    else if (shape == "decagon") {
        doDecagon();
    }
    else if (shape == "8p star") {
        doeightP_Star();
    }
    else if (shape == "6p star") {
        dosixP_Star();
    }
    else if (shape == "5p star") {
        dofiveP_Star();
    }
    else if (shape == "10p star") {
        dotenP_Star();
    }
    //else if (shape == "10p star") and (xangle != 0) {
   //     dotenP_Starrotate();
  //  }
    else if (shape == "10p star deep") {
        dotenP_StarDeep();
    }
    else {
        console.log("failed");
        doCircle();
    }
}

function setShape(s) {
    console.log(s);
    shape = s;
}

//---------------------------------------------

function tenP_StarDeeprotate(cx,cy) { 
    
    var c = document.getElementById('canvas');
    //var angle = 3.14/6;
    var angle = 3.14/(xangle);
    var rot = 4.71+angle;
    var x,y;
    var spikes=10;
    var step = 3.14/spikes;

    var ctx = c.getContext('2d');
    ctx.moveTo(cx,cy);

     for (i = 0; i <= spikes+1; i++) {
     x = cx + Math.cos(rot) * 100;
            y = cy + Math.sin(rot) * 100;
            ctx.lineTo(x, y);
            rot += step;

             x =cx + Math.cos(rot) * 50;
            y = cy + Math.sin(rot) * 50;
            ctx.lineTo(x, y);
            rot += step;
       }

   // ctx.fillStyle = '#f00';
    ctx.fill();
   
    
}

//---------------------------------------------

function tenP_Starrotate(cx,cy) { 
    
    var c = document.getElementById('canvas');
    //var angle = 3.14/6;
    var angle = 3.14/xangle;
    var rot = 4.71+angle;
    var x,y;
    var spikes=10;
    var step = 3.14/spikes;

    var ctx = c.getContext('2d');
    ctx.moveTo(cx,cy);

     for (i = 0; i <= spikes+1; i++) {
     x = cx + Math.cos(rot) * 50;
            y = cy + Math.sin(rot) * 50;
            ctx.lineTo(x, y);
            rot += step;

             x =cx + Math.cos(rot) * 38;
            y = cy + Math.sin(rot) * 38;
            ctx.lineTo(x, y);
            rot += step;
       }

    //ctx.fillStyle = "skyblue";
    ctx.fill();
   
    
}

//---------------------------------------------

function sixP_Starrotate(cx,cy) { 
    
var c = document.getElementById('canvas');
    //var angle = 3.14/6;
    var angle = 3.14/xangle;
    var rot = 4.71+angle;
    var x,y;
    var spikes=6;
    var step = 3.14/spikes;

    var ctx = c.getContext('2d');
    ctx.moveTo(cx,cy);

     for (i = 0; i <= spikes+1; i++) {
     x = cx + Math.cos(rot) * 50;
            y = cy + Math.sin(rot) * 50;
            ctx.lineTo(x, y);
            rot += step;

            x = cx + Math.cos(rot) * 30;
            y = cy + Math.sin(rot) * 30;
            ctx.lineTo(x, y);
            rot += step;
       }

   // ctx.fillStyle = '#f00';
    ctx.fill();    
}

//---------------------------------------------

function fiveP_Starrotate(cx,cy) { 
    
var c = document.getElementById('canvas');
    //var angle = 3.14/6;
    var angle = 3.14/xangle;
    var rot = 4.71+angle;
    var x,y;
    var spikes=5;
    var step = 3.14/spikes;

    var ctx = c.getContext('2d');
    ctx.moveTo(cx,cy);

     for (i = 0; i <= spikes+1; i++) {
     x = cx + Math.cos(rot) * 50;
            y = cy + Math.sin(rot) * 50;
            ctx.lineTo(x, y);
            rot += step;

            x = cx + Math.cos(rot) * 20;
            y = cy + Math.sin(rot) * 20;
            ctx.lineTo(x, y);
            rot += step;
       }

   //ctx.fillStyle = "skyblue";
    ctx.fill();    
}

//---------------------------------------------

function eightP_Starrotate(cx,cy) { 
    
var c = document.getElementById('canvas');
    //var angle = 3.14/6;
    var angle = 3.14/xangle;
    var rot = 4.71+angle;
    var x,y;
    var spikes=8;
    var step = 3.14/spikes;

    var ctx = c.getContext('2d');
    ctx.moveTo(cx,cy);

     for (i = 0; i <= spikes+1; i++) {
     x = cx + Math.cos(rot) * 50;
            y = cy + Math.sin(rot) * 50;
            ctx.lineTo(x, y);
            rot += step;

             x =cx + Math.cos(rot) * 38;
            y = cy + Math.sin(rot) * 38;
            ctx.lineTo(x, y);
            rot += step;
       }

   //ctx.fillStyle = "skyblue";
    ctx.fill();    
}

//---------------------------------------------

function hexagonrotate(cx,cy) { 
    var c = document.getElementById('canvas');  
    var ctx = c.getContext('2d');

//var cxt = document.getElementById('canvas').getContext('2d');
    var numberOfSides = 6;
    var size = 58;
    var Xcenter = cx;
    var Ycenter = cy;
    var angle = 3.14/xangle;
    
    ctx.beginPath();
    ctx.moveTo (Xcenter +  size * Math.cos(0), Ycenter +  size *  Math.sin(0)); 
//cxt.moveTo (Xcenter+size * Math.cos( 1 * 2 * Math.PI / numberOfSides),  Ycenter+size * Math.cos( 1 * 2 * Math.PI / numberOfSides)); 

for (var i = 1; i <= numberOfSides+1;i += 1) {
    ctx.lineTo (Xcenter + size * Math.cos(i * 2 * 3.14 / numberOfSides+3.14/numberOfSides+angle), Ycenter + size * Math.sin(i * 2 * 3.14 / numberOfSides+3.14/numberOfSides+angle));
}

//cxt.strokeStyle = "#000000";
//cxt.lineWidth = 1;
    //ctx.fillStyle = '#3F0BE4';
    ctx.fill();
    
}

//---------------------------------------------

function circle(x,y) {
    var c = document.getElementById("canvas");
    var ctx = c.getContext("2d");
    ctx.beginPath();
    ctx.arc(x, y, 50, 0, 2 * Math.PI);
    ctx.fillStyle = "#daa520";
    ctx.fill();
//ctx.stroke();

}

//document.getElementById("Circle").onclick = setShape("circle");

function doCircle() {
    circle(posX,posY);
}

//---------------------------------------------

function square(x,y) {
    var c = document.getElementById("canvas");
    var ctx = c.getContext("2d");
    //ctx.rect(x,y,100,100);
    //ctx.stroke();
    ctx.fillStyle = "#00008b";
    ctx.fillRect(x,y,100,100);
}

//document.getElementById("Square").onclick = setShape("square");

function doSquare() {
    /* if (eu_want_team === true) {
    //squarerotate(posX,posY);
         squareRotate2(posX,posY);
       return;
    }*/
    square(posX,posY);
}
 

//---------------------------------------------

function hexagon(x,y) {
    var c = document.getElementById("canvas");
    var ctx = c.getContext("2d");

    var numberOfSides = 6,
    size = 58;
    Xcenter = 100;
    Ycenter = 100;
    
    ctx.beginPath();
    ctx.moveTo (x +  size * Math.cos(0), y +  size *  Math.sin(0)); 
    
    for (var i = 1; i < numberOfSides;i += 1) {
	   ctx.lineTo (x + size * Math.cos(i * 2 * Math.PI / numberOfSides), y + size * Math.sin(i * 2 * Math.PI / numberOfSides));
    }
    ctx.closePath();
    ctx.fillStyle = "#3F0BE4";
    ctx.fill();
}

//document.getElementById("Hexagon").onclick = doHexagon;

function doHexagon() {
     if (eu_want_team === true) {
    hexagonrotate(posX,posY);
       return;
    }
    hexagon(posX,posY);
}
 
//---------------------------------------------

function decagon(x,y) {
    var c = document.getElementById("canvas");
    var ctx = c.getContext("2d");

    var numberOfSides = 10;
	size = 58;
	Xcenter = 100;
	Ycenter = 100;
    
    ctx.beginPath();
    ctx.moveTo (x +  size * Math.cos(0), y +  size *  Math.sin(0)); 
    
    for (var i = 1; i < numberOfSides;i += 1) {
	   ctx.lineTo (x + size * Math.cos(i * 2 * Math.PI / numberOfSides), y + size * Math.sin(i * 2 * Math.PI / numberOfSides));
    }
    ctx.closePath();
    ctx.fillStyle = "#02402b";
    ctx.fill();
}

//document.getElementById("Decagon").onclick = doDecagon;

function doDecagon() {
    decagon(posX,posY);
}

//---------------------------------------------

function fiveP_Star(cx, cy) {

    var c = document.getElementById("canvas");
    var ctx = c.getContext("2d");
    var rot = Math.PI / 2 * 3;
    var x = cx;
    var y = cy;
    var spikes =5;
    var outerRadius=50;
    var innerRadius=20;
    var step = Math.PI / spikes;

    ctx.beginPath();
    ctx.moveTo(cx, cy - outerRadius)
    for (i = 0; i < spikes; i++) {
        x = cx + Math.cos(rot) * outerRadius;
        y = cy + Math.sin(rot) * outerRadius;
        ctx.lineTo(x, y);
        rot += step;

        x = cx + Math.cos(rot) * innerRadius;
        y = cy + Math.sin(rot) * innerRadius;
        ctx.lineTo(x, y);
        rot += step;
    }
    
    ctx.lineTo(cx,cy-outerRadius);
    ctx.closePath();
    ctx.fillStyle = "#daa520";
    ctx.fill();

}

//document.getElementById("8P Star").onclick = doeightP_Star;

function dofiveP_Star() {
    if (eu_want_team === true) {
    fiveP_Starrotate(posX,posY);
       return;
    }
    
    fiveP_Star(posX,posY);
}

//---------------------------------------------
 
//---------------------------------------------

function eightP_Star(cx, cy) {

    var c = document.getElementById("canvas");
    var ctx = c.getContext("2d");
    var rot = Math.PI / 2 * 3;
    var x = cx;
    var y = cy;
    var spikes =8;
    var outerRadius=50;
    var innerRadius=38;
    var step = Math.PI / spikes;

    ctx.beginPath();
    ctx.moveTo(cx, cy - outerRadius)
    for (i = 0; i < spikes; i++) {
        x = cx + Math.cos(rot) * outerRadius;
        y = cy + Math.sin(rot) * outerRadius;
        ctx.lineTo(x, y);
        rot += step;

        x = cx + Math.cos(rot) * innerRadius;
        y = cy + Math.sin(rot) * innerRadius;
        ctx.lineTo(x, y);
        rot += step;
    }
    
    ctx.lineTo(cx,cy-outerRadius);
    ctx.closePath();
    ctx.fillStyle = "#36c49a";
    ctx.fill();

}

//document.getElementById("8P Star").onclick = doeightP_Star;

function doeightP_Star() {
    if (eu_want_team === true) {
    eightP_Starrotate(posX,posY);
       return;
    }
    
    eightP_Star(posX,posY);
}

//---------------------------------------------

function sixP_Star(cx, cy) {

    var c = document.getElementById("canvas");
    var ctx = c.getContext("2d");
    var rot = Math.PI / 2 * 3;
    var x = cx;
    var y = cy;
    var spikes =6;
    var outerRadius=50;
    var innerRadius=30;
    var step = Math.PI / spikes;

    ctx.beginPath();
    ctx.moveTo(cx, cy - outerRadius)
    for (i = 0; i < spikes; i++) {
        x = cx + Math.cos(rot) * outerRadius;
        y = cy + Math.sin(rot) * outerRadius;
        ctx.lineTo(x, y);
        rot += step;

        x = cx + Math.cos(rot) * innerRadius;
        y = cy + Math.sin(rot) * innerRadius;
        ctx.lineTo(x, y);
        rot += step;
    }
    
    ctx.lineTo(cx,cy-outerRadius);
    ctx.closePath();
    ctx.fillStyle = "skyblue";
    ctx.fill();

}

//document.getElementById("8P Star").onclick = doeightP_Star;

function dosixP_Star() {
    if (eu_want_team === true) {
    sixP_Starrotate(posX,posY);
       return;
    }
    
    sixP_Star(posX,posY);
}


//---------------------------------------------

function tenP_Star(cx, cy) {

    var c = document.getElementById("canvas");
    var ctx = c.getContext("2d");
    var rot = Math.PI / 2 * 3;
    var x = cx;
    var y = cy;
    var spikes =10;
    var outerRadius=50;
    var innerRadius=38;
    var step = Math.PI / spikes;

    ctx.beginPath();
    ctx.moveTo(cx, cy - outerRadius)
    for (i = 0; i < spikes; i++) {
        x = cx + Math.cos(rot) * outerRadius;
        y = cy + Math.sin(rot) * outerRadius;
        ctx.lineTo(x, y);
        rot += step;

        x = cx + Math.cos(rot) * innerRadius;
        y = cy + Math.sin(rot) * innerRadius;
        ctx.lineTo(x, y);
        rot += step;
    }
    
    ctx.lineTo(cx,cy-outerRadius);
    ctx.closePath();
    ctx.fillStyle = "skyblue";
    ctx.fill();

}

//document.getElementById("10P Star").onclick = dotenP_Star;

function dotenP_Star() {
     if (eu_want_team === true) {
    tenP_Starrotate(posX,posY);
       return;
    }
    
    tenP_Star(posX,posY);
    
}
//function dotenP_Starrotate() {
    
    
//}


//---------------------------------------------

function tenP_StarDeep(cx, cy) {

    var c = document.getElementById("canvas");
    var ctx = c.getContext("2d");
    //var rot = Math.PI / 2 * 3;
    var rot = Math.PI / 2 * 3;
    var x = cx;
    var y = cy;
    var spikes =10;
    var outerRadius=100;
    var innerRadius=50;
    var step = Math.PI / spikes;

    ctx.beginPath();
    ctx.moveTo(cx, cy - outerRadius)
    for (i = 0; i < spikes; i++) {
        //x = cx + Math.cos(rot) * outerRadius;
        x = cx + Math.cos(rot) * outerRadius;
        y = cy + Math.sin(rot) * outerRadius;
        ctx.lineTo(x, y);
        rot += step;

        x = cx + Math.cos(rot) * innerRadius;
        y = cy + Math.sin(rot) * innerRadius;
        ctx.lineTo(x, y);
        rot += step;
    }
    
    ctx.lineTo(cx,cy-(outerRadius));
    ctx.closePath();
    ctx.fillStyle = "#daa520";
    ctx.fill();

}

//document.getElementById("10P Star Deep").onclick = dotenP_StarDeep;

function dotenP_StarDeep() {
    if (eu_want_team === true) {
    tenP_StarDeeprotate(posX,posY);
       return;
    }
    tenP_StarDeep(posX,posY);
}

//-------------------------------------

var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].onclick = function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  }
}


//---------------------------------------------


/*function myFunction() {
    xangle = document.getElementById("myNumber").value;
    document.getElementById("demo").innerHTML = xangle;
}*/

function myFunction() {
     xangle = document.getElementById("myRange").value;
    document.getElementById("demo").innerHTML = xangle;
}

